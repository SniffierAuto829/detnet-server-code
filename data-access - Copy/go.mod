module data-access

go 1.17

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/lib/pq v1.10.4
)

require (
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
)

require (
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1
)
