package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	gomail "gopkg.in/mail.v2"

	_ "github.com/lib/pq"
)

const (
	db_username              = "tester"                   //username required to access db
	db_password              = "password"                 //password for db
	ip_address               = "172.20.2.20"              //ip address of server hosting db
	db_name                  = "alexdev"                  //name of database
	driver                   = "postgres"                 //driver used to connect to db
	table_Name               = "insert_tool_results"      //name of table within database
	db_port                  = 5432                       //port number of database
	sending_email_address    = "testdetnet1234@gmail.com" //account from which emails sent
	sending_address_password = "testing_email"            //password for email account
	smtp_Host                = "smtp.gmail.com"           //host for sending email address
	smtp_Port                = 587                        //port for sending email address
	JSON_read                = "example.json"             //name of JSON to be read
	//data_column              = "VALUE"                    //column in database to extract data points from to display to user
	date_column = "CREATED_AT" //column in database containing time information
	//value_column             = "DESCRIPTION"              //column in database that value entered by user compared to
	sys_id_column = "sys_id" //column in database containing system id
)

//struct to hold contents of JSON file
type Requested_JSON struct {
	Value      string `json:"value"`
	Statistic  string `json:"statistic"`
	Email      string `json:"email"`
	Start_Date string `json:"start_date"`
	End_Date   string `json:"end_date"`
	Sys_ID     string `json:"sys_id"`
}

//default value false, set to true when new value added to JSON file by user
//value set back to false when result of new query been handled
var got_new_input bool

//flag set to true when user selects to return all datapoints, false when chose specific stat
var get_all_data_points bool

func main() {

	start_time := time.Now() //get time at start of program
	get_all_data_points = false
	got_new_input = false   //default set value for new input query to false
	go initial_setup_http() //run as go routine to prevent it blocking rest of program
	process_data(start_time)

}

//process any new data inputted into JSON file
func process_data(start_time time.Time) {
	for { //infinite for loop to allow user to continuosly input requests

		//timer ensures program doesn't run indefinitely
		duration := time.Since(start_time)
		minutes := duration.Minutes()

		if minutes > 10 { //closes program after 10 minutes
			fmt.Println("Program timeout")
			os.Exit(3)
			//exit code 3 used to indicate program shutdown due to a timeout error
		}

		//got_new_input value altered in add_request() when user presses the submit button and their data
		//has been entered into the JSON file

		if got_new_input { //only runs if add_request() ran, user pressed submit button

			//establish connection to database using access criteria defined in constants
			db := connect_db()
			//open and read updated JSON file containg the users request, results stored in struct Requested_JSON
			User_Request := read_JSON()

			//create SQL statement using info defined in JSON file
			SQL_statement := make_SQL(User_Request)
			var result_of_query_data []float32
			var result_of_query_date []time.Time

			//run the SQL query and save the result
			if get_all_data_points {
				length_query := run_SQL_counter(User_Request.Value, User_Request.Start_Date, User_Request.End_Date, db, User_Request.Sys_ID)
				result_of_query_data, result_of_query_date = run_SQL_all_data(SQL_statement, db, length_query)
				//email field of struct will by "EXCEL" if user has requested to download excel data
				if User_Request.Email == "EXCEL" {
					save_excel_value_and_date(result_of_query_data, result_of_query_date, User_Request.Value, User_Request.Statistic, User_Request.Sys_ID)
				} else {
					send_email(result_of_query_data, User_Request)
				}
			} else {
				result_of_query_data = run_SQL(SQL_statement, db)
				if User_Request.Email == "EXCEL" {
					save_excel_value(result_of_query_data, User_Request.Statistic, User_Request.Value, User_Request.Sys_ID)
				} else {
					send_email(result_of_query_data, User_Request)
				}
			}
			db.Close() //close connection to database
		}
	}
}

//Setup http access at localhost:3000 to load add_request.html which takes inputs that are sent through to the JSON
func initial_setup_http() {

	//handler for submit button press, calls add_request()
	// "/add_request" is name of "submit" button in HTML form
	//add_request function called when url of http://localhost:3000/add_request passed
	http.HandleFunc("/add_request", add_request)

	//handler for "end program" button press, calls add_request()
	// "/add_request" is name of "end program" button in HTML form
	http.HandleFunc("/end_program", end_program)

	//calls open method when http://localhost:3000 passed
	http.HandleFunc("/", open)
	fmt.Println("Listening on :3000...")

	//HTTP server started by ListenAndServe at http://localhost:3000
	//parameter nil indicates that DefaultServeMux is used
	err_serve_http := http.ListenAndServe(":3000", nil)
	if err_serve_http != nil {
		log.Fatal(err_serve_http)
	}
}

//Handler for initial_http_setup Handlefunc above.
//Reads in the JSON, appends the user inputted data to the old JSON and overwrites the JSON file
func add_request(w http.ResponseWriter, r *http.Request) {

	var email_entered string
	var JSON_entered Requested_JSON
	retrieval_method := r.FormValue("retrieval") //either excel or email represent user choice on how to view request
	if retrieval_method == "EXCEL" {             //contents of email checked in main() to call either save_excel() or send_email()
		email_entered = "EXCEL"
	} else {
		email_entered = r.FormValue("email_address") //store email address entered in HTML form
	}

	start_date := r.FormValue("start_date")
	start_date = strings.ReplaceAll(start_date, "T", " ")
	end_date := r.FormValue(("end_date"))
	end_date = strings.ReplaceAll(end_date, "T", " ")

	//fetch remainder of values inputted into HTML form, common for both retrieval methods
	value_entered := r.FormValue("value_selected")
	stat_entered := r.FormValue("stat")
	sys_id := r.FormValue("sys_id")

	//add all user entered values to struct Requested_JSON
	JSON_entered.Email = email_entered
	JSON_entered.Statistic = stat_entered
	JSON_entered.Value = value_entered
	JSON_entered.Start_Date = start_date
	JSON_entered.End_Date = end_date
	JSON_entered.Sys_ID = sys_id

	//contents of JSON_entered, marshalled to convert from struct type into json(Byte data)
	dataBytes, err_marshalling := json.Marshal(JSON_entered)
	if err_marshalling != nil {
		log.Fatal(err_marshalling)
	}

	//write dataBytes to the JSON file, JSON_read
	//0644 used to overwrite the information currently in the JSON file
	err_write := ioutil.WriteFile(JSON_read, dataBytes, 0644)
	if err_write != nil {
		log.Fatal(err_write)
	}

	//indicates unprocessed data in the JSON file, allows the main() to establish connection to the database and process the data
	got_new_input = true
	//reload the html form back to its original values allowing user to enter more data
	http.Redirect(w, r, "add_request.html", http.StatusFound)
}

//Opens the file User_interface.html
func open(w http.ResponseWriter, r *http.Request) {
	temp_site, err_open_html := template.ParseFiles("User_interface.html")
	//Parse the html file "User_interface.html" on variable temp_site

	//applies template to the http.ResponseWriter w which is used to construct an HTTP response
	temp_site.Execute(w, nil)

	//parsed template temp_site written to the io.Writer wr
	// "User_interface.html" will be shown when http://localhost.3000 run in browser
	if err_open_html != nil {
		panic(err_open_html)
	}
}

//Establish connection with database using defined constants
func connect_db() (db *sql.DB) {
	db_access := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", ip_address, db_port,
		db_username, db_password, db_name)
	//db_access is database dsn in format needed for postgres database
	//format host = ip_address port = db_port user = db_username password = db_password dbname = db_name sslmode = disable

	//Open() validates arguements provided to connect to database, doesn't actially connect to database
	db, err_db_access := sql.Open(driver, db_access)
	if err_db_access != nil {
		log.Fatal(err_db_access)
	}

	//Ping() used to ensure connection to the database exists, or create a connection if one doesn't exist
	err_db_ping := db.Ping()
	if err_db_ping != nil {
		panic(err_db_ping)
	}
	return db //returns pointer to connection to database
}

//Read given JSON file and separate into value, stat and email variables
func read_JSON() (contents_JSON Requested_JSON) {
	//open the JSON file defined in constants to be read
	jsonFile, err_JSON_read := os.Open(JSON_read)

	if err_JSON_read != nil {
		panic(err_JSON_read)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile) //read all the contents of the JSON file
	//var final_JSON_request Requested_JSON

	//& points to the memory address of final_JSON_request
	json.Unmarshal(byteValue, &contents_JSON) //convert json(Byte data) to struct and save at memory address of final_JSON_request
	return contents_JSON
}

//Use info from JSON file to create required SQL statement
func make_SQL(UserRequest Requested_JSON) (sql_statement string) {

	stat := UserRequest.Statistic
	value := UserRequest.Value
	start_date := UserRequest.Start_Date
	end_date := UserRequest.End_Date
	sys_id := UserRequest.Sys_ID
	//sys_id = UserRequest.Sys_ID
	if stat == "ALL" {
		get_all_data_points = true //set flag to use functions that add both date and time to excel document
		sql_statement = fmt.Sprintf("SELECT %v,%v FROM public.%v WHERE %v = %v AND %v BETWEEN '%v' AND '%v';", value, date_column, table_Name, sys_id_column, sys_id, date_column, start_date, end_date)
		//SQL statement to get all values from given columnName in table_Name that match the user's inputted value
		//SELECT "columnName" From public."table_Name" WHERE "system_id_column" = "sys_id" AND "date_column" BETWEEN "start_date" AND "end date"
	} else {
		get_all_data_points = false //set flag to use functions that only use value
		//sql_statement = fmt.Sprintf("SELECT %v(%v) FROM public.%v WHERE %v ~'%v' AND %v BETWEEN '%v' AND '%v';", stat, data_column, table_Name, value_column, value, date_column, start_date, end_date)
		sql_statement = fmt.Sprintf("SELECT %v(%v) FROM public.%v WHERE %v=%v AND %v BETWEEN '%v' AND '%v';", stat, value, table_Name, sys_id_column, sys_id, date_column, start_date, end_date)
		//SQL statement to get user desired stat from given columnName in table_Name that match user's inputted value
		//SELECT "stat"("value") FROM public."table_Name" WHERE "system_id_column = "sys_id AND "date_column" BETWEEN "start_date" AND "end_date";
	}
	return sql_statement
}

func run_SQL_counter(value string, start_date string, end_date string, db *sql.DB, sys_id string) (number_items_query int) {
	statement := fmt.Sprintf("SELECT COUNT(%v) From public.%v WHERE %v = %v AND %v BETWEEN '%v' AND '%v';", value, table_Name, sys_id_column, sys_id, date_column, start_date, end_date)
	rows, err_running_sql := db.Query(statement) //execute given SQL statement returns results in datatype rows
	if err_running_sql != nil {
		log.Fatal(err_running_sql)
	}
	for rows.Next() { //for loop to run through the contents of the SQL statement results
		err_scanning := rows.Scan(&number_items_query)
		if err_scanning != nil {
			log.Fatal(err_scanning)
		}
	}
	return number_items_query
}

//Run SQL script on created link to database db
func run_SQL_all_data(SQL_statement string, db *sql.DB, number_results int) (query_result_data []float32, query_result_time []time.Time) {

	query_result_data = make([]float32, 0, number_results)
	query_result_time = make([]time.Time, 0, number_results)

	var row_value float32 //variable to temporarily hold value from a row
	var row_time time.Time

	rows, err_running_sql := db.Query(SQL_statement) //execute given SQL statement returns results in datatype rows
	if err_running_sql != nil {
		log.Fatal(err_running_sql)
	}
	defer rows.Close()
	for rows.Next() { //for loop to run through the contents of the SQL statement results
		err_scanning := rows.Scan(&row_value, &row_time)
		// & used to give the memory address of row_value
		//scan used to move through the coloumns of the rows
		//SQL statement should only return one column, this value is saved in the memory address of row_value
		if err_scanning != nil {
			log.Fatal(err_scanning)
		}
		query_result_time = append(query_result_time, row_time)
		query_result_data = append(query_result_data, row_value) // add value from row to slice of all row values
	}
	err_rows := rows.Err()
	if err_rows != nil {
		log.Fatal(err_rows)
	}
	return query_result_data, query_result_time // return the slice containing all rows returned by the SQL statement
}

//run SQL function to handle statements that only return value without date
func run_SQL(SQL_statement string, db *sql.DB) (query_results []float32) {

	query_results = make([]float32, 0, 1)
	var row_value float32 //variable to temporarily hold value from a row

	rows, err_running_sql := db.Query(SQL_statement) //execute given SQL statement returns results in datatype rows
	if err_running_sql != nil {
		log.Fatal(err_running_sql)
	}
	defer rows.Close()
	for rows.Next() { //for loop to run through the contents of the SQL statement results
		err_scanning := rows.Scan(&row_value)
		// & used to give the memory address of row_value
		//scan used to move through the coloumns of the rows
		//SQL statement should only return one column, this value is saved in the memory address of row_value
		if err_scanning != nil {
			log.Fatal(err_scanning)
		}
		query_results = append(query_results, row_value) // add value from row to slice of all row values
	}
	err_rows := rows.Err()
	if err_rows != nil {
		log.Fatal(err_rows)
	}
	return query_results // return the slice containing all rows returned by the SQL statement
}

//Save result of the SQL query to an excel file. Handles case of SQL statement returning date/time as well as value
//Function can handle both a single value result as well as an array of results.
//Each result will be displayed on a new row in column A of sheet 1
func save_excel_value_and_date(result_of_query_data []float32, result_of_query_date []time.Time, value string, stat string, sys_id string) {

	f := excelize.NewFile() //create new file
	f.SetCellValue("Sheet1", "A1", "Date")
	f.SetCellValue("Sheet1", "B1", value)
	//for loop to run through []float32 array of results
	f.SetColWidth("Sheet1", "A", "A", 20) //set column width to fit date/time data
	for i := 0; i < len(result_of_query_data); i++ {

		cell_date := fmt.Sprintf("A%v", i+2)
		f.SetCellValue("Sheet1", cell_date, result_of_query_date[i])

		cell_data := fmt.Sprintf("B%v", i+2)                         //location of next empty cell in column A sheet 1 A1 already occupied by header so start at A2
		f.SetCellValue("Sheet1", cell_data, result_of_query_data[i]) //put result in next empty cell
	}

	//default file name of saved excel sheet is: Query result "stat" "value"
	//"stat" and "value" are the user's inputs from struct Requested_JSON
	fileName := fmt.Sprintf("System %v,Query result %v %v.xlsx", sys_id, stat, value)
	err_file_save := f.SaveAs(fileName) //file saved
	if err_file_save != nil {
		log.Fatal(err_file_save)
	}
	got_new_input = false //contents of JSON file have been processed, so variable set to false
}

//save result to from SQL query to excel
//function handles case when only value returned without the date & time
func save_excel_value(result_of_query_data []float32, stat string, value string, sys_id string) {
	header := stat + " " + value //make heading for column A of the excel sheet

	f := excelize.NewFile() //create new file
	f.SetCellValue("Sheet1", "A1", header)
	//for loop to run through []float32 array of results
	f.SetColWidth("Sheet1", "A", "A", 20) //set column width to fit date/time data
	for i := 0; i < len(result_of_query_data); i++ {
		cell_data := fmt.Sprintf("A%v", i+2)                         //location of next empty cell in column A sheet 1 A1 already occupied by header so start at A2
		f.SetCellValue("Sheet1", cell_data, result_of_query_data[i]) //put result in next empty cell
	}

	//default file name of saved excel sheet is: Query result "stat" "value"
	//"stat" and "value" are the user's inputs from struct Requested_JSON
	fileName := fmt.Sprintf("System %v, Query result %v %v.xlsx", sys_id, stat, value)
	err_file_save := f.SaveAs(fileName) //file saved
	if err_file_save != nil {
		log.Fatal(err_file_save)
	}
	got_new_input = false //contents of JSON file have been processed, so variable set to false
}

//Create email body using JSON info and send to given email address from email address defined in constants
func send_email(value []float32, data Requested_JSON) {

	//data validation of email entry happens in html form
	recieving_email := data.Email
	stat := data.Statistic
	stat_long := expand_stat_explanation(stat) //convert stat to long form to improve email readability
	email_body := ""
	email_subject := ""
	start_fixed := data.Start_Date
	end_fixed := data.End_Date
	if stat == "ALL" { //ALL requires different email body since returns more than one data point
		email_subject = fmt.Sprintf("All the data for %v from %v to %v", data.Value, start_fixed, end_fixed)
		email_body = fmt.Sprintf("All the data over %v to %v for %v is: %v", start_fixed, end_fixed, data.Value, value)
	} else {
		email_subject = fmt.Sprintf("Analysis of %v, %v, from %v to %v", data.Value, stat_long, start_fixed, end_fixed)
		email_body = fmt.Sprintf("The %v between %v and %v for %v is: %v", stat_long, start_fixed, end_fixed, data.Value, value[0])
	}

	mail := gomail.NewMessage()                      //create new email
	mail.SetHeader("From", sending_email_address)    //set email address the email is sent from
	mail.SetAddressHeader("To", recieving_email, "") //set email address email is sent to
	mail.SetHeader("Subject", email_subject)         //set subject of email
	mail.SetBody("text/html", email_body)            //set body of email

	dial := gomail.NewDialer(smtp_Host, smtp_Port, "testdetnet1234@gmail.com", "testing_email")
	//create new dialer, paramaters used to connect to the smtp port
	err_sending_email := dial.DialAndSend(mail)
	//open connection to SMTP server, send email configure above and then close connection to SMTP server

	if err_sending_email != nil {
		panic(err_sending_email)
	}
	got_new_input = false //new input to JSON file has been handled so variable set to false
}

//Convert SQL definition of required stat to full English version to improve email read ability
func expand_stat_explanation(short_stat string) (long_stat string) {
	switch short_stat {
	case "MAX":
		long_stat = "maximum value"
	case "MIN":
		long_stat = "minimum value"
	case "AVG":
		long_stat = "average value"
	case "STDDEV_SAMP":
		long_stat = "standard deviation"
	case "ALL":
		long_stat = "all"
	}
	return long_stat
}

//Shut down program and http connection immediately when end_program button pressed
func end_program(w http.ResponseWriter, r *http.Request) {
	os.Exit(0)
}
